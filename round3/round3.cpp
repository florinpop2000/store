#include<iostream>
#include<fstream>
#include<vector>
#include<sstream>
#include"product.h"
#include<algorithm>
#include<regex>
#include"Nonperishable.h"
#include"Perishable.h"


//bool compareByName(const Product& product1, const Product& product2)
//{
//	return (product1.getName() < product2.getName());
//}

/*void showProducts(const std::vector<Product>& products, std::ostream& outputStream)
{
	for (const Product& p : products)
	{
		if (p.isPersihable())
		{
			outputStream << p.getName() << " " << p.getPrice() << " " << p.getVAT() << " " << p.getType() << " Total price: " << p.getTotalPrice() << "\n";
		}
	}
}*/
bool isPersihable(const std::string& type) 
{
	static const std::regex isDate("\\d\\d\\d\\d-\\d\\d-\\d\\d");
	return std::regex_match(type, isDate);
}

NonperishableProductType getNonperishableProductType(const std::string& type)
{
	if (type == "Clothing")
	{
		return NonperishableProductType::Clothing;
	}
	else if (type == "SmallAppliences")
	{
		return NonperishableProductType::SmallAppliences;
	}
	else if (type == "PersonalHygiene")
	{
		return NonperishableProductType::PersonalHygiene;
	}
	throw "Invalid type string";
}
bool copareAscendingByPrice(IPriceable* price1, IPriceable* price2)
{
	return price1->getPrice() < price2->getPrice();
}

int main()
{
	std::ifstream file("product.prodb");
	std::vector <IPriceable*> products; 

	int id;
	uint16_t vat;
	std::string name;
	float price;
	std::string type;

	std::string currentLine;
	while (std::getline(file, currentLine)) {
		std::istringstream lineParser(currentLine);
		lineParser >> id >> name >> price >> vat >> type;
		if (isPersihable(type))
		{
			products.push_back(new Perishable(id, name, price, type, vat));
		}
		else
		{

			products.push_back(new Nonperishable(id, name, price, getNonperishableProductType(type), vat));
		}
	}
	for (int index = 0; index < products.size(); ++index)
	{
		if (auto item=dynamic_cast<Nonperishable*>(products[index]); item )
		{
			std::cout << item->getName() << " " << item->getPrice() << std::endl;
		}
	}
	std::sort(products.begin(),products.end(), copareAscendingByPrice);
	for (int index = 0; index < products.size(); ++index)
	{
			std::cout << products[index]->getPrice() << "\n";
	}

	auto nameComparator = [](IPriceable* lefNt, IPriceable* right) 
	{
		return	dynamic_cast<Product*>(left)->getName() < dynamic_cast<Product*>(right)->getName();
	};
	std::sort(products.begin(), products.end(), nameComparator);
	for (int index = 0; index < products.size(); ++index)
	{
		std::cout << dynamic_cast<Product*>(products[index])->getName() << "\n";
		delete products[index];
	}
	/*while (!file.eof())
	{
		file >> id >> name >> price >> vat >> type;
		product product1(id, name, price, vat, type);
		products.push_back(product1);
	}*/
	//std::sort(std::begin(products), std::end(products), compareByName);
	std::ofstream outputfilestream1("out1.txt");
	//showProducts(products, outputfilestream1);
	/*for (const product& p : products)
	{
		if (p.isPersihable())
		{
			std::cout << p.getName() << " " << p.getPrice() << " " << p.getVAT() << " " << p.getType() <<" Total price: "<<p.getTotalPrice()<< "\n";
		}
	}*/
	//auto priceComparator = [](const Product& product1, Product& product2) { return product1.getPrice()<product2.getPrice(); };
	//std::sort(std::begin(products), std::end(products), priceComparator);
	//std::cout << std::endl;
	//std::ofstream outputfilestream2("out2.txt");
	//showProducts(products, outputfilestream2);
	/*for (const product& p : products)
	{
		if (p.isPersihable())
		{
			std::cout << p.getName() << " " << p.getPrice() << " " << p.getVAT() << " " << p.getType() << " Total price: " << p.getTotalPrice() << "\n";
		}
	}*/
	return 0;
}