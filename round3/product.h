#pragma once
#include"IPriceable.h"
#include<string>
class Product: public IPriceable
{


public:
	Product(int id, std::string name, float price, uint16_t VAT);

	//float getPrice() const;
	//uint16_t getVAT() const;
	const std::string& getName() const;
	//const std::string& getType() const;
	//bool isPersihable() const;
	float getTotalPrice() const;

	// Inherited via IPriceable
	uint16_t getVAT() override;

	float getPrice() override;

protected:
	int m_Id;
	uint16_t m_VAT;
	std::string m_Name;
	float m_Price;
	//std::string m_Type;

};