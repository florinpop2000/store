#pragma once
#include "product.h"
class Perishable : public Product
{
public:
	Perishable(int id, const std::string& name, float rawPrice, const std::string& expirationDate,uint16_t VAT);
	const std::string& getExpirationDate() const;
private:
	std::string m_ExpirationDate;
	
};

