#pragma once

#include<cstdint>

class IPriceable
{
public:
	virtual uint16_t getVAT()=0;
	virtual float getPrice()=0;
	virtual ~IPriceable()= default;
};
