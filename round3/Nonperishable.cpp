#include "Nonperishable.h"

Nonperishable::Nonperishable(int id, const std::string& name, float rawPrice, NonperishableProductType productType, uint16_t VAT):
	Product(id,name,rawPrice,VAT),
	m_productType(productType)
{
}

const NonperishableProductType Nonperishable::getType() const
{
	return m_productType;
}
