#include "product.h"
#include <regex>

Product::Product(int id, std::string name, float price, uint16_t VAT) :
	m_Id(id),
	m_Name(name),
	m_Price(price),
	m_VAT(VAT)
{}

/*float Product::getPrice() const
{
	return m_Price;
}

uint16_t Product::getVAT() const
{
	return m_VAT;
}*/

const std::string& Product::getName() const
{
	return m_Name;
}

/*const std::string& Product::getType() const
{
	return m_Type;
}*/

/*bool Product::isPersihable() const
{
	std::regex isDate("\\d\\d\\d\\d-\\d\\d-\\d\\d");
	return std::regex_match(m_Type, isDate);
}*/

float Product::getTotalPrice() const
{
	return m_Price+m_VAT/100.0*m_Price;
}

uint16_t Product::getVAT()
{
	return m_VAT;
}

float Product::getPrice()
{
	return m_Price + m_VAT / 100.0 * m_Price;
}
