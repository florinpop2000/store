#include "Perishable.h"

Perishable::Perishable(int id, const std::string& name, float rawPrice, const std::string& expirationDate, uint16_t VAT) :
	Product(id, name, rawPrice, VAT),
	m_ExpirationDate(expirationDate)
{
}

const std::string& Perishable::getExpirationDate() const
{
	return m_ExpirationDate;
}
