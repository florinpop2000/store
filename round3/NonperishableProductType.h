#pragma once
#include<cstdint>
enum class NonperishableProductType: uint8_t
{
	Clothing,
	SmallAppliences,
	PersonalHygiene
};
