#pragma once
#include "product.h"
#include "NonperishableProductType.h"
class Nonperishable : public Product
{
	public:
		Nonperishable(int id, const std::string& name, float rawPrice, NonperishableProductType productType, uint16_t VAT);
		const NonperishableProductType getType() const;
private:
	NonperishableProductType m_productType;
};

